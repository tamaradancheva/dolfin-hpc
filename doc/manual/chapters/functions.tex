\chapter{Functions}
\index{functions}
\index{Function}

\devnote{Since this chapter was written, the \texttt{Function} class has
  seen a number of improvements which are not covered here. Chapter needs
  to be updated.}

The central concept of a function on a domain $\Omega \subset \R^d$ is
modeled by the class \texttt{Function}, which is used in \dolfin{} to
represent coefficients or solutions of partial differential equations.

%------------------------------------------------------------------------------
\section{Basic properties}

The following basic properties hold for all \texttt{Function}s:
\begin{itemize}
\item
  A \texttt{Function} can be scalar or vector-valued;
\item
  A \texttt{Function} can be restricted (interpolated) to each local
  \texttt{Cell} of a \texttt{Mesh};
\item
  The underlying representation of a \texttt{Function} may vary.
\end{itemize}

Depending on the actual underlying representation of a \texttt{Function}, it
may also be possible to evaluate a \texttt{Function} at any given \texttt{Point}.

\subsection{Representation}

Currently supported representations of \texttt{Function}s include
\emph{discrete} \texttt{Function}s, \emph{user-defined}
\texttt{Function}s and \emph{constant} \texttt{Function}s. These are
discussed in detail below.

\subsection{Assignment}

One \texttt{Function} may be assigned to another \texttt{Function}:
\begin{code}
Function v;
Function u = v;
\end{code}

\subsection{Output}
\index{ParaView}
\index{MayaVi}

A \texttt{Function} can be written to a file in various file formats.
To write a \texttt{Function}~\texttt{u} to file in VTK~format,
suitable for viewing in ParaView or MayaVi, create a file with
extension \texttt{.pvd}:
\begin{code}
File file("solution.pvd");
file << u;
\end{code}

For further details on available file formats, see
Chapter~\ref{chapter:io}.

%------------------------------------------------------------------------------
\section{Discrete functions}

A discrete \texttt{Function} is defined in terms of a
\texttt{Vector} of degrees of freedom, a \texttt{Mesh}, a
local-to-global mapping (\texttt{DofMap}) and a finite element.  In
particular, a discrete \texttt{Function} is given by a linear
combinations of basis functions:
\begin{equation}
  v = \sum_{i=1}^{N} v_i \phi_{i},
\end{equation}
where $\{\phi_i\}_{i=1}^N$ is the global basis of the finite element
space defined by the \texttt{Mesh} and the finite element, and
the nodal values $\{v_i\}_{i=1}^N$ are given by the values of a
\texttt{Vector}.

%------------------------------------------------------------------------------
\section{User-defined functions}
\index{user-defined functions}

In the simplest case, a user-defined \texttt{Function} is just an
expression in terms of the coordinates and is typically used for
defining source terms and initial conditions. For example, a source
term could be given by
\begin{equation} \label{eq:functionexample}
  f = f(x, y, z) = xy \sin(z / \pi).
\end{equation}

A user-defined \texttt{Function} may be defined by creating a sub
class of \texttt{Function} and overloading the \texttt{eval()}
function.  The following example illustrates how to create a
\texttt{Function} representing the function in
(\ref{eq:functionexample}):
\begin{code}
class Source : public Function
{
public:
    
  Source(Mesh& mesh) : Function(mesh) {}

  real eval(const real* x) const
  {
    return x[0]*x[1]*sin(x[2] / DOLFIN_PI);
  }

};

Source f;
\end{code}

\devnote{Write about how to define vector-valued functions.}

\subsection{Cell-dependent functions}

In some cases, it may be convenient to define a \texttt{Function} in
terms of properties of the current \texttt{Cell}. One such example is
a \texttt{Function} that at any given point takes the value of the
mesh size at that point.

The following example illustrates how to create such as
\texttt{Function} by overloading the \texttt{eval()} function:
\begin{code}
class MeshSize : public Function
{
public:

  MeshSize(Mesh& mesh) : Function(mesh) {}

  real eval(const real* x) const
  {
    return cell().diameter();
  }
    
};

MeshSize h;
\end{code}

Note that the current \texttt{Cell} is only available during assembly
and has no meaning otherwise. For example, it is not possible to write the
\texttt{Function}~\texttt{h} to file.


