\chapter{The log system}
\index{log system}

\dolfin{} provides provides a simple interface for uniform handling of
log messages, including warnings and errors. All messages are
collected to a single stream, which allows the destination and
formatting of the output from an entire program, including the
\dolfin{} library, to be controlled by the user.

%------------------------------------------------------------------------------
\section{Generating log messages}
\index{\texttt{message()}}
\index{\texttt{cout}}
\index{\texttt{endl}}

Log messages can be generated using the function
\texttt{message()} available in the \texttt{dolfin} namespace:
\begin{code}
void message(std::string format, ...);
\end{code}
which works similarly to the standard C library function \texttt{printf}.
The following examples illustrate the usage of
\texttt{message()}:
\begin{code}
message("Solving linear system.");
message("Size of vector: \%d.", x.size());
message("R = \%.3e (TOL = \%.3e)", R, TOL);
\end{code}

As an alternative to \texttt{message()}, \dolfin{} provides a C++
style interface to generating log messages. Thus, the above examples
can also be implemented as follows:
\footnotesize
\begin{code}
cout << "Solving linear system." << endl;
cout << "Size of vector: " << x.size() << "." << endl;
cout << "R = " << R << " (TOL = " << TOL << ")" << endl;
\end{code}
\normalsize
Note the use of \texttt{dolfin::cout} and
\texttt{dolfin::endl} from the \texttt{dolfin} namespace,
corresponding to the standard standard \texttt{std::cout} and
\texttt{std::endl} in namespace \texttt{std}. If log messages are
directed to standard output (see below), then \texttt{dolfin::cout}
and \texttt{std::cout} may be mixed freely.

Most classes provided by \dolfin{} can be used together with
\texttt{dolfin::cout} and \texttt{dolfin::endl} to display short
informative messages about objects:
\begin{code}
Matrix A(10, 10);
cout << A << endl;
\end{code}
To display detailed information for an object,  use the member function
\texttt{disp()}:
\begin{code}
Matrix A(10, 10);
A.disp();
\end{code}
Use with caution for large objects. For a \texttt{Matrix}, calling
\texttt{disp()} displays all matrix entries.

%------------------------------------------------------------------------------
\section{Warnings and errors}
\index{warnings}
\index{errors}
\index{\texttt{warning()}}
\index{\texttt{error()}}

Warnings and error messages can be generated using the functions
\begin{code}
warning(std::string format, ...);
error(std::string format, ...);
\end{code}
Once an error is encountered, the program throws an exception.

%------------------------------------------------------------------------------
\section{Debug messages and assertions}
\index{debugging}
\index{assertions}
\index{\texttt{dolfin\_debug()}}
\index{\texttt{dolfin\_assert()}}

The macro \texttt{dolfin\_debug()} works similarly to
\texttt{message()}:
\begin{code}
dolfin_debug(message);
\end{code}
but in addition to displaying the given message, information is printed about
the location of the code that generated the debug message (file,
function name and line number).

Note that in order to pass formatting strings and additional arguments
with debug messages, the variations \texttt{dolfin\_debug1()},
\texttt{dolfin\_debug2()} and so on, depending on the number of
arguments, must be used.

Assertions can often be a helpful programming tool. Use assertions
whenever you assume something about about a variable in your code,
such as assuming that given input to a function is valid. \dolfin{}
provides the macro \texttt{dolfin\_assert()} for creating assertions:
\begin{code}
dolfin_assert(check);
\end{code}
This macro accepts a boolean expression and if the expression
evaluates to false, an error message is displayed, including the
file, function name and line number of the assertion, and a
segmentation fault is raised (to enable easy attachment to a
debugger). The following examples illustrate the use of
\texttt{dolfin\_assert()}:
\begin{code}
dolfin_assert(i >= 0);
dolfin_assert(i < n);
dolfin_assert(cell.type() == Cell::triangle);
dolfin_assert(cell.type() == Cell::tetrahedron);
\end{code}
Note that assertions are only active when compiling
\dolfin{} and your program with \texttt{DEBUG} defined (configure
option \texttt{--enable-debug} or compiler flag \texttt{-DDEBUG}).
Otherwise, the macro \texttt{dolfin\_assert()} expands to nothing,
meaning that liberal use of assertions does not affect performance,
since assertions are only present during development and
debugging.

%------------------------------------------------------------------------------
\section{Task notification}
\index{tasks}
\index{\texttt{dolfin\_begin()}}
\index{\texttt{dolfin\_end()}}

The two functions \texttt{dolfin\_begin()} and \texttt{dolfin\_end()}
available in the \texttt{dolfin} name space can be used to notify the
\dolfin{} log system about the beginning and end of a task:
\begin{code}
void begin();
void end();
\end{code}
Alternatively, a string message (or a formatting string with optional
arguments) can be supplied:
\begin{code}
void begin(std::string format, ...);
void end();
\end{code}

These functions enable the \dolfin{} log system to display messages,
warnings and errors hierarchically, by automatically indenting the
output produced between calls to \texttt{begin()} and
\texttt{end()}. A program may contain an arbitrary number of nested
tasks.

%------------------------------------------------------------------------------
\section{Progress bars}
\index{progress bar}
\index{\texttt{Progress}}

The \dolfin{} log system provides the class \texttt{Progress} for
simple creation of progress sessions. A progress session automatically
displays the progress of a computation using a progress bar.

If the number of steps of a computation is known, a progress session
should  be defined in terms of the number of steps and updated in each
step of the computation as illustrated by the following example:
\begin{code}
Progress p("Assembling", mesh.noCells());  
for (CellIterator c(mesh); !c.end(); ++c)
{
  ...
  p++;
}
\end{code}
It is also possible to specify the step number explicitly by assigning
an integer to the progress session:
\begin{code}
Progress p("Iterating over vector", x.size())
for (uint i = 0; i < x.size(); i++)
{
  ...
  p = i;
}
\end{code}

Alternatively, if the number of steps is unknown, the progress session
needs to be updated with the current percentage of the progress:
\begin{code}
Progress p("Time-stepping");
while ( t < T )
{
  ...
  p = t / T;
}
\end{code}

The progress bar created by the progress session will only be updated
if the progress has changed significantly since the last update (by
default at least $10\%$). The
amount of change needed for an update can be controlled using the
parameter \texttt{"progress step"}:
\begin{code}
dolfin_set("progress step", 0.01);
\end{code}

Note that several progress sessions may be created simultaneously, or
nested within tasks.

%------------------------------------------------------------------------------
\section{Controlling the destination of output}
\index{output destination}

By default, the \dolfin{} log system directs messages to standard
output (the terminal). Messages may also be turned off completely.
To specify the destination, set the value of the parameter "output destination":
\begin{code}
dolfin_set("output destination", "terminal");
dolfin_set("output destination", "silent");
\end{code}

One may also set the debug level for the \dolfin{} log system so that
only messages with a debug level higher than or equal to the current
debug level are printed:
\begin{code}
dolfin_set("debug level", "1");
\end{code}
