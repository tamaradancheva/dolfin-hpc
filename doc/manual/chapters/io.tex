\chapter{Input/output}
\label{chapter:io}
\index{input/output}
\index{I/O}
\index{pre-processing}
\index{post-processing}

\dolfin{} relies on external programs for pre- and post-processing,
which means that computational meshes must be imported from file
(pre-processing) and computed solutions must be exported to file and
then imported into another program for visualization
(post-processing). To simplify this process, \dolfin{} provides
support for easy interaction with files and includes output formats
for a number of visualization programs.

%------------------------------------------------------------------------------
\section{Files and objects}
\index{object}
\index{\texttt{File}}

A file in \dolfin{} is represented by the class \texttt{File} and
reading/writing data is done using the standard C++ operators
\texttt{>>} (read) and \texttt{<<} (write).

Thus, if \texttt{file} is a \texttt{File} and \texttt{object} is an
object of some class that can be written to file, then the object can
be written to file as follows:
\begin{code}
file << object;
\end{code}
Similarly, if \texttt{object} is an object of a class that can be read
from file, then data can be read from file (overwriting any previous
data held by the object) as follows:
\begin{code}
file >> object;
\end{code}

The format (type) of a file is determined by its filename suffix, if
not otherwise specified. Thus, the following code creates a
\texttt{File} for reading/writing data in \dolfin{} XML format:
\begin{code}
File file("data.xml");
\end{code}
A complete list of file formats and corresponding file name suffixes
is given in Table~\ref{tab:formats}.

Alternatively, the format of a file may be explicitly defined. One may
thus create a file named \texttt{data.xml} for reading/writing data in
GNU~Octave format:
\begin{code}
File file("data.xml", File::octave);
\end{code}

\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{|l|l|l|}
      \hline
      Suffix & Format & Description \\
      \hline
      \hline
      \texttt{.xml}/\texttt{.xml.gz} & \texttt{File::xml} & \dolfin{} XML \\
      \hline
      \texttt{.bin}  & \texttt{File::binary} & \dolfin{} Binary \\
      \hline
      \texttt{.pvd} & \texttt{File::vtk} & VTK \\
      \hline
      \texttt{.m} & \texttt{File::octave} & GNU Octave \\
      \hline
      (\texttt{.m}) & \texttt{File::matlab} & MATLAB \\
      \hline
    \end{tabular}
    \caption{File formats and corresponding file name suffixes.}
    \label{tab:formats}
  \end{center}
\end{table}

Although many of the classes in \dolfin{} support file input/output,
it is not supported by all classes and the support varies with the
choice of file format. A summary of supported classes/formats is
given in Table~\ref{tab:classes,formats}.

\begin{table}[htbp]
  \begin{center}
    \begin{tabular}{|l||c|c|c|c|c|}
      \hline
      Format           & \texttt{Vector} & \texttt{Matrix} & \texttt{Mesh} & \texttt{Function}  & \texttt{Parallel}\\
      \hline
      \hline
      \texttt{File::xml}     & in/out & in/out & in/out & --- & Partial \\
      \hline
      \texttt{File::binary}     & in/out & --- & in/out & in/out & Yes\\
      \hline
      \texttt{File::vtk}     & ---    & ---    & out    & out  & Yes \\
      \hline
      \texttt{File::octave}  & out    & out    & out    & out  & No \\
      \hline
      \texttt{File::matlab}  & out    & out    & out    & out  & No\\
      \hline
    \end{tabular}
    \caption{Matrix of supported combinations of classes and file
      formats for input/output in \dolfin{}.}
    \label{tab:classes,formats}
  \end{center}
\end{table}

%------------------------------------------------------------------------------
\section{File formats}
\index{file formats}

In this section, we give some pointers to each of the file formats
supported by \dolfin{}. For detailed information, we refer to the
respective user manual of each format/program.

\devnote{This section needs to be improved and expanded. Any
  contributions are welcome.}

\subsection{\dolfin{} XML}
\index{XML}

\dolfin{} XML is the native format of \dolfin{}. As the name says,
data is stored in XML ASCII format. This has the advantage of being a
robust and human-readable format, and if the files are compressed
there is little overhead in terms of file size compared to a binary
format.

\dolfin{} automatically handles gzipped XML files, as
illustrated by the following example which reads a \texttt{Mesh} from
a compressed \dolfin{} XML file and saves the mesh to an uncompressed
\dolfin{} XML file:
\begin{code}
Mesh mesh;

File in("mesh.xml.gz");
in >> mesh;

File out("mesh.xml");
out << mesh;
\end{code}
The same thing can of course be accomplished by
\begin{code}
# gunzip -c mesh.xml.gz > mesh.xml
\end{code}
on the command-line.

\subsection{\dolfin{} Binary}
\index{DOLFIN Binary}

\dolfin{} Binary is the new native HPC format of \dolfin{} and the
only format with support for parallel I/O. As the name says, data is
stored in uncompressed binary with some additionally header
information. A complete specification of the file format can be found
in appendix \ref{app:dolfin_binary}

\subsection{VTK}

Data saved in VTK format~\cite{www:VTK} can be visualized using various
packages. The powerful and freely available ParaView~\cite{www:ParaView} 
is recommended. Alternatively, VTK data can be visualized in 
MayaVi~\cite{www:MayaVi}, which is recommended for quality vector PostScript 
output. Time-dependent data is handled automatically in the VTK format.

The below code illustrates how to export a function in VTK format:
\begin{code}
Function u;

File out("data.pvd");
out << u;
\end{code}
The sample code produces the file \texttt{data.pvd}, which can be read 
by ParaView. The file \texttt{data.pvd} contains a list of files which 
contain the results computed by \dolfin{}. For the above example, these 
files would be named \texttt{dataXXX.vtu}, where \texttt{XXX} is a counter 
which is incremented each time the function is saved. If the function 
\texttt{u} was to be saved three times, the files
\begin{code}
data000000.vtu
data000001.vtu
data000002.vtu
\end{code}
would be produced. Individual snapshots can be visualized by opening the 
desired file with the extension~\texttt{.vtu} or~\texttt{.pvd} using ParaView.

\subsection{GNU Octave}

GNU~Octave~\cite{www:Octave} is a free clone of MATLAB that can be
used to visualize solutions computed in \dolfin{}, using the commands
\texttt{pdemesh}, \texttt{pdesurf} and \texttt{pdeplot}. These
commands are normally not part of GNU~Octave but are
provided by \dolfin{} in the subdirectory \texttt{src/utils/octave/} of
the \dolfin{} source tree. These commands require the external program
\texttt{ivview} included in the open source distribution of
Open~Inventor~\cite{www:OpenInventor}. (Debian users install the
package \texttt{inventor-clients}.)

To visualize a solution computed with \dolfin{} and exported in
GNU~Octave format, first load the solution into GNU~Octave by just
typing the name of the file without the \texttt{.m} suffix. If the
solution has been saved to the file \texttt{poisson.m}, then just type
\begin{code}
octave:1> poisson
\end{code}
The solution can now be visualized using the command
\begin{code}
octave:2> pdesurf(points, cells, u)
\end{code}
or to visualize just the mesh, type
\begin{code}
octave:3> pdesurf(points, edges, cells)
\end{code}

\subsection{MATLAB}

Since MATLAB~\cite{www:MATLAB} is not free, users are encouraged to
use GNU~Octave whenever possible. That said, data is visualized in
much the same way in MATLAB as in GNU~Octave, using the
MATLAB~commands \texttt{pdemesh}, \texttt{pdesurf} and
\texttt{pdeplot}.

%------------------------------------------------------------------------------
\section{Converting between file formats}

\dolfin{} supplies a script for easy conversion between different file
formats.  The script is named \texttt{dolfin-convert} and can be found
in the directory \texttt{src/utils/convert/} of the \dolfin{} source
tree. The only supported file formats are currently the Medit
\texttt{.mesh} format, the Gmsh \texttt{.msh} version 2.0 format and the 
\dolfin{} XML (\texttt{.xml}) mesh format.

To convert a mesh in Medit \texttt{.mesh} format generated by TetGen 
with the -g option, type
\begin{code}
# dolfin-convert mesh.mesh mesh.xml
\end{code}
%
To convert a mesh in Gmsh \texttt{.msh} format type 
\begin{code}
# dolfin-convert mesh.msh mesh.xml
\end{code}
In generating a Gmsh mesh, make sure to define a 
physical surface/volume.
%
It is also possible to convert a mesh from the old \dolfin{} XML
(\texttt{.xml}) mesh format to the current one by typing
\begin{code}
# dolfin-convert -i xml-old old_mesh.xml new_mesh.xml
\end{code}
%
Example meshes can be found in the directory
\texttt{src/utils/convert/} of the \dolfin{} source tree.

%------------------------------------------------------------------------------
\section{A note on new file formats}

With some effort, \dolfin{} can be expanded with new file formats. Any
contributions are welcome. If you wish to contribute to \dolfin{},
then adding a new file format (or improving upon an existing file
format) is a good place to start. Take a look at one of the current
formats in the subdirectory \texttt{src/kernel/io/} of the \dolfin{}
source tree to get a feeling for how to design the file format, or ask
at \texttt{dolfin-dev@fenics.org} for directions.

Also consider contributing to the \texttt{dolfin-convert} script by
adding a conversion routine for your favorite format. The script is
written in Python and should be easy to extend with new formats.g
