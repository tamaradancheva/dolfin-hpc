#ifndef __DOLFIN_QUADRATURE_H
#define __DOLFIN_QUADRATURE_H

// DOLFIN quadrature interface

#include <dolfin/quadrature/Quadrature.h>
#include <dolfin/quadrature/GaussianQuadrature.h>
#include <dolfin/quadrature/GaussQuadrature.h>
#include <dolfin/quadrature/RadauQuadrature.h>
#include <dolfin/quadrature/LobattoQuadrature.h>

#endif
