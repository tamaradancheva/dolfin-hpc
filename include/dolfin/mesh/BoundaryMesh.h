// Copyright (C) 2006-2008 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2006-06-21
// Last changed: 2008-05-28

#ifndef __BOUNDARY_MESH_H
#define __BOUNDARY_MESH_H

#include <dolfin/common/types.h>
#include "Mesh.h"
#include "MeshFunction.h"

namespace dolfin
{

  /// A BoundaryMesh is a mesh over the boundary of some given mesh.

  class BoundaryMesh : public Mesh
  {
  public:

    /// Create an empty boundary mesh
    BoundaryMesh();

    /// Create boundary mesh from given mesh
    BoundaryMesh(Mesh& mesh);

    /// Destructor
    ~BoundaryMesh();

    /// Initialize boundary mesh
    void init(Mesh& mesh);

    /// Initialize boundary mesh (incl. facets between processors)
    void init_local(Mesh& mesh);

    /// Initialize interior boundary mesh (facets between processors)
    void init_interior(Mesh& mesh);

  };

}

#endif
