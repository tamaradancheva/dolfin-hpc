#ifndef __DOLFIN_MESH_H
#define __DOLFIN_MESH_H

// DOLFIN mesh interface

#include <dolfin/mesh/ALE.h>
#include <dolfin/mesh/ALEType.h>
#include <dolfin/mesh/MeshEntity.h>
#include <dolfin/mesh/MeshEntityIterator.h>
#include <dolfin/mesh/MeshTopology.h>
#include <dolfin/mesh/MeshGeometry.h>
#include <dolfin/mesh/MeshData.h>
#include <dolfin/mesh/MeshConnectivity.h>
#include <dolfin/mesh/MeshEditor.h>
#include <dolfin/mesh/MeshFunction.h>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/MPIMeshCommunicator.h>
#include <dolfin/mesh/LoadBalancer.h>
#include <dolfin/mesh/Vertex.h>
#include <dolfin/mesh/Edge.h>
#include <dolfin/mesh/Face.h>
#include <dolfin/mesh/Facet.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/mesh/Point.h>
#include <dolfin/mesh/SubDomain.h>
#include <dolfin/mesh/DomainBoundary.h>
#include <dolfin/mesh/BoundaryMesh.h>
#include <dolfin/mesh/UnitCube.h>
#include <dolfin/mesh/UnitInterval.h>
#include <dolfin/mesh/UnitSquare.h>
#include <dolfin/mesh/UnitCircle.h>
#include <dolfin/mesh/Box.h>
#include <dolfin/mesh/Rectangle.h>
#include <dolfin/mesh/UnitSphere.h>
#include <dolfin/mesh/IntersectionDetector.h>
#include <dolfin/mesh/RivaraRefinement.h>

#endif
