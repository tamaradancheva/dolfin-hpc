// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2006-06-07
// Last changed: 2006-06-16

#ifndef __UNIFORM_MESH_REFINEMENT_H
#define __UNIFORM_MESH_REFINEMENT_H

#include <dolfin/common/types.h>

#include <dolfin/mesh/MeshFunction.h>


#ifdef HAVE_LIBGEOM
namespace libgeom
{
	class Geometry;
}
#endif

namespace dolfin
{
  
  class Mesh;
  
  /// This class implements uniform mesh refinement for different mesh types.
  
  class UniformMeshRefinement
  {
  public:
    
    /// Refine mesh uniformly according to mesh type
    static void refine(Mesh& mesh);
    
#ifdef HAVE_LIBGEOM
    /// Refine mesh uniformly according to mesh type overloaded method to include geometry informations surfaces
    static void refine(Mesh& mesh, libgeom::Geometry& geom, 
		       MeshFunction<int>& patch_id_list,
		       MeshFunction<float>& bnd_u,
		       MeshFunction<float>& bnd_v );
    
    /// Refine mesh uniformly according to mesh type overloaded method to include geometry informations curves
    static void refine(Mesh& mesh, libgeom::Geometry& geom, 
		       MeshFunction<int>& patch_id_list, 
		       MeshFunction<float>& bnd_u);
#endif
    
    /// Refine simplicial mesh uniformly
    static void refineSimplex(Mesh& mesh);
    
#ifdef HAVE_LIBGEOM
    /// Refine simplicial mesh uniformly overloaded method to include geometry informations surfaces
    static void refineSimplex(Mesh& mesh, libgeom::Geometry& geom, 
			      MeshFunction<int>& patch_id_list,
			      MeshFunction<float>& bnd_u, 
			      MeshFunction<float>& bnd_v );
    
    /// Refine simplicial mesh uniformly overloaded method to include geometry informations curves
    static void refineSimplex(Mesh& mesh, libgeom::Geometry& geom, 
			      MeshFunction<int>& patch_id_list,
			      MeshFunction<float>& bnd_u );
#endif 
  };

}

#endif
