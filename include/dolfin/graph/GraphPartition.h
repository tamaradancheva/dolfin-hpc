// Copyright (C) 2007 Magnus Vikstrom
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2007-04-03
// Last changed: 2007-04-22

#ifndef __GRAPH_PARTITIONING_H
#define __GRAPH_PARTITIONING_H

#include <dolfin/common/types.h>
#include "Graph.h"

namespace dolfin
{
  /// This class provides a set of functions to partition a Graph

  class GraphPartition
  {
  public:
    
    /// Partition a graph into num_part partitions
    static void partition(Graph& graph, uint num_part, uint* vtx_part);

    /// Check partition correctness
    static void check(Graph& graph, uint num_part, uint* vtx_part);

    /// Evaluate partition quality
    static void eval(Graph& graph, uint num_part, uint* vtx_part);

    /// Display partitioning
    static void disp(Graph& graph, uint num_part, uint* vtx_part);
	 
    /// Calculate edge_cut
    static uint edgecut(Graph& graph, uint num_part, uint* vtx_part);

  };

}

#endif
