#ifndef __DOLFIN_COMMON_H
#define __DOLFIN_COMMON_H

// DOLFIN common classes

#include <dolfin/common/types.h>
#include <dolfin/common/constants.h>
#include <dolfin/common/timing.h>
#include <dolfin/common/Array.h>
#include <dolfin/common/List.h>
#include <dolfin/common/simple_array.h>
#include <dolfin/common/Timer.h>
#include <dolfin/common/TimeDependent.h>
#include <dolfin/common/Variable.h>

#endif
