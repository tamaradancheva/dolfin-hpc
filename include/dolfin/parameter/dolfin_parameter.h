#ifndef __DOLFIN_PARAMETER_H
#define __DOLFIN_PARAMETER_H

/// DOLFIN parameter interface

#include <dolfin/parameter/Parametrized.h>
#include <dolfin/parameter/Parameter.h>
#include <dolfin/parameter/ParameterSystem.h>
#include <dolfin/parameter/parameters.h>

#endif
