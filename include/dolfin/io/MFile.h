// Copyright (C) 2003-2008 Johan Hoffman and Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2003-07-15
// Last changed: 2008-03-29

#include <dolfin/config/dolfin_config.h>

#ifndef __M_FILE_H
#define __M_FILE_H

#include <dolfin/common/types.h>
#include "GenericFile.h"

namespace dolfin
{
  
  class MFile : public GenericFile
  {
  public:
    
    MFile(const std::string filename);
    virtual ~MFile();

    // Input
    
    // Output

    void operator<< (GenericVector& x);
    virtual void operator<< (GenericMatrix& A) = 0;
    void operator<< (Mesh& mesh);
    void operator<< (Function& u);

  };
  
}

#endif

