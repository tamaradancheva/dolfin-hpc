// Copyright (C) 2002-2006 Johan Hoffman and Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// Modified by Garth N. Wells 2005, 2006.
// Modified by Haiko Etzel 2005.
// Modified by Magnus Vikstrom 2007.
// Modified by Nuno Lopes 2008
// Modified by Niclas Jansson, 2008-2012.
//
// First added:  2002-11-12
// Last changed: 2012-05-11

#include <dolfin/config/dolfin_config.h>

#include <string>
#include <dolfin/log/dolfin_log.h>
#include <dolfin/main/MPI.h>
#include <dolfin/io/File.h>
#include <dolfin/io/GenericFile.h>
#include <dolfin/io/XMLFile.h>
#include <dolfin/io/BinaryFile.h>
#include <dolfin/io/MatlabFile.h>
#include <dolfin/io/OctaveFile.h>
#include <dolfin/io/VTKFile.h>
#include <dolfin/io/RAWFile.h>
#include <dolfin/io/STLFile.h>
#include <dolfin/io/XYZFile.h>

using namespace dolfin;

//-----------------------------------------------------------------------------
File::File(const std::string& filename)
{
  // Choose file type base on suffix.

  // FIXME: Use correct funtion to find the suffix; using rfind() makes
  // FIXME: it essential that the suffixes are checked in the correct order.

  if ( filename.rfind(".xml") != filename.npos )
#ifdef HAVE_XML
    file = new XMLFile(filename);
#else
    error("DOLFIN is not built with XML support");
#endif
  else if ( filename.rfind(".xml.gz") != filename.npos )
#ifdef HAVE_XML
    file = new XMLFile(filename);
#else
    error("DOLFIN is not built with XML support");
#endif
  else if ( filename.rfind(".bin") != filename.npos)
    file = new BinaryFile(filename);
  else if ( filename.rfind(".m") != filename.npos )
    file = new OctaveFile(filename);
  else if ( filename.rfind(".pvd") != filename.npos )
    file = new VTKFile(filename);
  else if ( filename.rfind(".raw") != filename.npos )
    file = new RAWFile(filename);
  else if ( filename.rfind(".stl") != filename.npos )
    file = new STLFile(filename);
  else if ( filename.rfind(".xyz") != filename.npos )
    file = new XYZFile(filename);
  else
  {
    file = 0;
    error("Unknown file type for \"%s\".", filename.c_str());
  }
}
//-----------------------------------------------------------------------------
File::File(const std::string& filename, Type type)
{
  switch ( type ) {
#ifdef HAVE_XML
  case xml:
    file = new XMLFile(filename);
    break;
#endif
  case binary:
    file = new BinaryFile(filename);
    break;
  case matlab:
    file = new MatlabFile(filename);
    break;
  case octave:
    file = new OctaveFile(filename);
    break;
  case vtk:
    file = new VTKFile(filename);
    break;
  default:
    file = 0;
    error("Unknown file type for \"%s\".", filename.c_str());
  }
}
//-----------------------------------------------------------------------------
File::File(const std::string& filename, real& t)
{

  if ( filename.rfind(".pvd") != filename.npos )
    file = new VTKFile(filename, t);
  else if( filename.rfind(".bin") != filename.npos)
    file = new BinaryFile(filename , t);
  else
  {
    file = 0;
    error("Unknown file type for time dependent \"%s\".", filename.c_str());
  }
}
//-----------------------------------------------------------------------------
File::~File()
{
  if ( file )
    delete file;
  file = 0;
}
//-----------------------------------------------------------------------------
void File::operator>>(GenericVector& x)
{
  file->read();
  
  *file >> x;
}
//-----------------------------------------------------------------------------
void File::operator>>(GenericMatrix& A)
{
  file->read();
  
  *file >> A;
}
//-----------------------------------------------------------------------------
void File::operator>>(Mesh& mesh)
{
  file->read();
  
  *file >> mesh;
}
//-----------------------------------------------------------------------------
void File::operator>>(MeshFunction<int>& meshfunction)
{
  file->read();
  
  *file >> meshfunction;
}
//-----------------------------------------------------------------------------
void File::operator>>(MeshFunction<unsigned int>& meshfunction)
{
  file->read();
  
  *file >> meshfunction;
}
//-----------------------------------------------------------------------------
void File::operator>>(MeshFunction<double>& meshfunction)
{
  file->read();
  
  *file >> meshfunction;
}
//-----------------------------------------------------------------------------
void File::operator>>(MeshFunction<bool>& meshfunction)
{
  file->read();
  
  *file >> meshfunction;
}
//-----------------------------------------------------------------------------
void File::operator>>(Function& f)
{
  file->read();
  
  *file >> f;
}
//-----------------------------------------------------------------------------
void File::operator>>(ParameterList& parameters)
{
  file->read();
  
  *file >> parameters;
}
//-----------------------------------------------------------------------------
void File::operator>>(Graph& graph)
{
  file->read();
  
  *file >> graph;
}
//-----------------------------------------------------------------------------
void File::operator>>(std::vector<std::pair<Function*, std::string> >& f) 
{
  file->read();

  *file >> f;
}
//-----------------------------------------------------------------------------
void File::operator<<(GenericVector& x)
{
  file->write();
  
  *file << x;
}
//-----------------------------------------------------------------------------
void File::operator<<(GenericMatrix& A)
{
  file->write();
	 
  *file << A;
}
//-----------------------------------------------------------------------------
void File::operator<<(Mesh& mesh)
{
  file->write();
  
  *file << mesh;
}
//-----------------------------------------------------------------------------
void File::operator<<(MeshFunction<int>& meshfunction)
{
  file->write();
  
  *file << meshfunction;
}
//-----------------------------------------------------------------------------
void File::operator<<(MeshFunction<unsigned int>& meshfunction)
{
  file->write();
  
  *file << meshfunction;
}
//-----------------------------------------------------------------------------
void File::operator<<(MeshFunction<double>& meshfunction)
{
  file->write();
  
  *file << meshfunction;
}
//-----------------------------------------------------------------------------
void File::operator<<(MeshFunction<bool>& meshfunction)
{
  file->write();
  
  *file << meshfunction;
}
//-----------------------------------------------------------------------------
void File::operator<<(Function& u)
{
  file->write();
  
  *file << u;
}
//-----------------------------------------------------------------------------
void File::operator<<(ParameterList& parameters)
{
  file->write();
  
  *file << parameters;
}
//-----------------------------------------------------------------------------
void File::operator<<(Graph& graph)
{
  file->write();
  
  *file << graph;
}
//-----------------------------------------------------------------------------
void File::operator<<(std::vector<std::pair<Function*, std::string> >& f) 
{
  file->write();

  *file << f;
}
//-----------------------------------------------------------------------------
void File::set_counter(uint new_value)
{
  file->set_counter(new_value);
}
//-----------------------------------------------------------------------------
