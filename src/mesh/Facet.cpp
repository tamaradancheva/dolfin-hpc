// Copyright (C) 2006 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2006-06-02
// Last changed: 2006-06-02

#include <dolfin/mesh/Facet.h>

using namespace dolfin;
